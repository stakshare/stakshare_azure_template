#!/usr/bin/env bash
sudo apt-get -y update 
#don't do apt-get upgrade because it does not work with AWS
sudo sysctl -w vm.nr_hugepages=128

git clone https://bitbucket.org/stakshare/stakshare --depth 1

cd stakshare

chmod u+x run_stak.pl

#get finminer
wget https://github.com/nanopool/nanominer/releases/download/v1.1.1/nanominer-linux-1.1.1.tar.gz
tar xzvf nanominer-linux-1.1.1.tar.gz
mv nanominer-linux-1.1.1 nanominer
cd nanominer
rm config.ini
cp ../miner_config.ini config.ini
